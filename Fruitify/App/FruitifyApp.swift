//
//  FruitifyApp.swift
//  Fruitify
//
//  Created by Daksh Desai on 21/05/21.
//

import SwiftUI

@main
struct FruitifyApp: App {
    
    @AppStorage("isOnboarding") var isOnboarding: Bool = true
    
    var body: some Scene {
        WindowGroup {
            if isOnboarding {
                OnboardingView()
            }
            else{
                ContentView()
            }
        }
    }
}
