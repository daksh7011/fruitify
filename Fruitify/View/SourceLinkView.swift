//
//  SourceLinkView.swift
//  Fruitify
//
//  Created by Daksh Desai on 22/05/21.
//
//  Follow me on Twitter: @SlothieSmooth
//
    

import SwiftUI

struct SourceLinkView: View {
    
    // MARK: - PROPERTIES
    
    
    
    // MARK: - Body
    
    var body: some View {
        GroupBox() {
            HStack{
                Text("Content Source")
                Spacer()
                Link("Wikipedia",destination: URL(string: "https://wikipedia.com")!)
                Image(systemName: "arrow.up.right.square")
            }
            .font(.footnote)
        } //: GroupBox
    }
}

// MARK: - Preview

struct SourceLinkView_Previews: PreviewProvider {
    static var previews: some View {
        SourceLinkView()
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
