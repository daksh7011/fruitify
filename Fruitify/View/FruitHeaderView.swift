//
//  FruitHeaderView.swift
//  Fruitify
//
//  Created by Daksh Desai on 22/05/21.
//
//  Follow me on Twitter: @SlothieSmooth
//


import SwiftUI

struct FruitHeaderView: View {
    
    // MARK: - PROPERTIES
    
    var fruit: Fruit
    
    // MARK: - Body
    
    var body: some View {
        ZStack {
            LinearGradient(
                gradient: Gradient(colors: fruit.gradientColors),
                startPoint: .topLeading,
                endPoint: .bottomTrailing
            )
            Image(fruit.image)
                .resizable()
                .scaledToFit()
                .shadow(color: Color(red: 0, green: 0, blue: 0,opacity: 0.15), radius: 8, x: 6, y: 8 )
                .padding(.vertical,20)
                .animation(nil)
                .scaleEffect()
                .animation(.easeOut)
        } //: Zstack
        .frame(height: 440)
    }
}

// MARK: - Preview

struct FruitHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        FruitHeaderView(fruit: fruitData[0])
            .previewLayout(.fixed(width: 375, height: 440))
    }
}
